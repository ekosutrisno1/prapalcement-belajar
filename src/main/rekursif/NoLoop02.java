package main.rekursif;

import java.util.Scanner;

public class NoLoop02 {
   public static void main(String[] args) {
      Scanner input = new Scanner(System.in);
      print("Basis: ");
      int basis = input.nextInt();
      print("Pangkat: ");
      int pangkat = input.nextInt();
      System.out.print("Hasil (" + basis + "^" + pangkat + ") : ");
      prinf(pangkatFunction(basis, pangkat));
   }

   private static int pangkatFunction(int basis, int pangkat) {
      int angka = 1;
      if (pangkat > 0) {
         angka = basis * pangkatFunction(basis, pangkat - 1);
      }
      return angka;
   }

   private static int pangkatFunctionSingkat(int basis, int pangkat) {
      if (pangkat > 0) return basis * pangkatFunction(basis, pangkat - 1);
      return 1;
   }


   private static void prinf(int angka) {
      System.out.printf("%d ", angka);
   }

   private static void print(String string) {
      System.out.print(string);
   }
}
