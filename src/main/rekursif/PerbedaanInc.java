package main.rekursif;


import java.util.Arrays;

public class PerbedaanInc {
   public static void main(String[] args) {
   /*perbedaan antara n++ dengan ++n adalah
   jika n++ return nilai awal kemudian di incrementkan
   sedangal ++n diincremencakn dahulu kemunidian return nilai yang baru
   */

      //contoh n++ nya pada kasus berikut:
      int a = 0;
      int[] arr = new int[5];
      for (int i = 0; i < arr.length; i++) {
         arr[i] = a++;
      }
      System.out.print(Arrays.toString(arr));

      enter(); //buat enter
      //contoh n++ nya pada kasus berikut:
      int b = 0;
      int[] arr1 = new int[5];
      for (int i = 0; i < arr.length; i++) {
         arr1[i] = ++b;
      }
      System.out.print(Arrays.toString(arr1));
   }

   private static void enter() {
      System.out.println();
   }
}
