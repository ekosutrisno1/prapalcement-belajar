package main.rekursif;

public class Faktorial {
   public static void main(String[] args) {
      int hasil = out100(4);
      System.out.println(hasil);
   }

   private static int out100(int banyak) {
      if (banyak == 0 || banyak == 1)
         return 1;
      else
         return banyak * out100(banyak - 1);
   }

}
