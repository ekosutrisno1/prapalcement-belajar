package main.rekursif;

public class NoLoop01 {
   public static void main(String[] args) {
      cetakangka(100);
   }

   private static void cetakangka(int angka) {
      if (angka > 0)
         cetakangka(angka - 1);
      prinf(angka);
   }

   private static void prinf(int angka) {
      System.out.printf("%d ", angka);
   }
}
