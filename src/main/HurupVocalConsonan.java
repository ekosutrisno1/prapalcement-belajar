package main;

import java.util.Arrays;
import java.util.Scanner;

public class HurupVocalConsonan {
   public static void main(String[] args) {
      Scanner scan = new Scanner(System.in);
      System.out.print("Masukkan Kalimat: ");
      String kata = scan.nextLine();

      char[] vocal = printVocal(kata).toCharArray();
      char[] consonan = printConsonan(kata).toCharArray();
      int jVoc = jumlahVocal(kata);
      int jCon = jumlahConsonan(kata);

      System.out.println("Huruf Vokal: " + Arrays.toString(vocal));
      System.out.println("Huruf Consonan: " + Arrays.toString(consonan));
      System.out.println("Result=> vocal : " + jVoc + ", consonan : " + jCon + ", total : " + (jVoc + jCon) + " hurup.");

      scan.close();
   }

   // 01. mencetak hurup vokal
   private static String printVocal(String input) {
      char[] kata = input.toCharArray();
      StringBuilder result = new StringBuilder();

      for (char c : kata) {
         if (c == 'a' || c == 'i' || c == 'u' || c == 'e' || c == 'o') {
            result.append(c);
         }
      }
      return result.toString();
   }
//^[aeiouAEIOU].*[aeiouAEIOU]$
   // 02. Mencetak hurup konsonan
   private static String printConsonan(String input) {
      char[] kata = input.toCharArray();
      StringBuilder result = new StringBuilder();
      for (char c : kata) {
         if (!(c == 'a' || c == 'i' || c == 'u' || c == 'e' || c == 'o')) {
            if (c == ' ' || c == '.' || c == '"' || c == '_' || c == ';' || c == ',' || c == '(' || c == ')' || c == ':' || c == '-') {
               continue;
            }
            result.append(c);
         }
      }
      return result.toString();
   }

   // 03. melihat jumlah hurup vocal
   private static int jumlahVocal(String input) {
      char[] data = input.toCharArray();
      int bVokal = 0;
      for (char c : data) {
         if (c == 'a' || c == 'i' || c == 'u' || c == 'e' || c == 'o') {
            bVokal++;
         }
      }
      return bVokal;
   }

   //  04. melihat jumlah hurup konsonan
   private static int jumlahConsonan(String input) {
      char[] data = input.toCharArray();
      int bConsonan = 0;
      for (char c : data) {
         if (!(c == 'a' || c == 'i' || c == 'u' || c == 'e' || c == 'o')) {
            if (c == ' ' || c == '.' || c == ';' || c == '"' || c == '_' || c == ',' || c == '(' || c == ')' || c == ':' || c == '-') {
               continue;
            }
            bConsonan++;
         }
      }
      return bConsonan;
   }

}
