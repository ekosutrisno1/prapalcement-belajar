package main.regex;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Regex10 {
   public static void main(String[] args) {
      Scanner inputan = new Scanner(System.in);

      boolean kondisi = true;
      int banyak = 0;
      while (kondisi) {
         System.out.print("Masukkan email: ");
         String email = inputan.nextLine();
         boolean tes = isValid(email);
         System.out.println(tes);
         banyak++;
         if (banyak == 5)
            kondisi = false;
      }


   }

   private static boolean isValid(String email) {
      String EMAIL_REGEX = "^[\\w-+]+(\\.[\\w]+)*@[\\w-]+(\\.[\\w]+)*(\\.[a-z]{2,})$";
      Pattern pattern = Pattern.compile(EMAIL_REGEX, Pattern.CASE_INSENSITIVE);
      Matcher matcher = pattern.matcher(email);
      return matcher.matches();
   }
}
