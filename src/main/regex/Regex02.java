package main.regex;

import java.util.Scanner;

public class Regex02 {
   public static void main(String[] args) {
      Scanner inputan = new Scanner(System.in);
      System.out.print("Masukkan password: ");
      String password = inputan.nextLine();
      boolean tes = cekPassword(password);
      System.out.println(tes);
   }

   private  static boolean cekPassword(String password){
      String regex = "^[a-z0-9_-]{6,18}$";
      return password.matches(regex);
   }
}
