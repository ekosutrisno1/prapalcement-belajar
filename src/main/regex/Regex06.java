package main.regex;

import java.util.Scanner;

public class Regex06 {
   public static void main(String[] args) {
      Scanner inputan = new Scanner(System.in);
      System.out.print("Masukkan url: ");
      String url = inputan.nextLine();
      boolean tes = cekUrl(url);
      System.out.println(tes);
   }

   private static boolean cekUrl(String url){
      String regex ="^(https?://)?([\\da-z.-]+)\\.([a-z.]{2,6})([/\\w .-]*)*/?$";
      return url.matches(regex);
   }
}
