package main.regex;

import java.util.Scanner;

public class Regex04 {
   public static void main(String[] args) {
      Scanner inputan = new Scanner(System.in);
      System.out.print("Masukkan slug/url: ");
      String slug = inputan.nextLine();
      boolean tes = cekSlug(slug);
      System.out.println(tes);
   }

   private static boolean cekSlug(String slug){
      String regex = "^[a-z0-9-]+$";
      return slug.matches(regex);
   }
}
