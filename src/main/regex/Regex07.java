package main.regex;

import java.util.Scanner;

public class Regex07 {
   public static void main(String[] args) {
      Scanner inputan = new Scanner(System.in);
      System.out.print("Masukkan alamat IP: ");
      String ip = inputan.nextLine();
      boolean tes = cekAlamatIP(ip);
      System.out.println(tes);
   }

   private static boolean cekAlamatIP(String ip){
      String regex ="^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$";
      return ip.matches(regex);
   }
}
