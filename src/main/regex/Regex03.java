package main.regex;

import java.util.Scanner;

public class Regex03 {
   public static void main(String[] args) {
      Scanner inputan = new Scanner(System.in);
      System.out.print("Masukkan kode heksadesimal: ");
      String heksa = inputan.nextLine();
      boolean tes = cekHeksa(heksa);
      System.out.println(tes);
   }

   private  static boolean cekHeksa(String heksa){
      String regex = "^#?([a-f0-9]{6}|[a-f0-9]{3})$";
      return heksa.matches(regex);
   }
}
