package main.regex;

import java.util.Scanner;

public class Regex05 {
   public static void main(String[] args) {
      Scanner inputan = new Scanner(System.in);
      System.out.print("Masukkan email: ");
      String email = inputan.nextLine();
      boolean tes = cekEmail(email);
      boolean tes1 = cekEmail1(email);
      System.out.println(tes + " <=> " + tes1);
   }

   private static boolean cekEmail(String email) {
      String regex = "^([a-z0-9_.-]+)@([\\da-z.-]+)\\.([a-z.]{2,6})$";
      return email.matches(regex);
   }

   private static boolean cekEmail1(String email) {
      String regex = "^(([^<>()\\[\\]\\\\.,;:\\s@\"]+(\\.[^<>()\\[\\]\\\\.,;:\\s@\"]+)*)|(\".+\"))@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
      return email.matches(regex);
   }

}
