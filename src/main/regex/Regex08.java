package main.regex;

import java.util.Scanner;

public class Regex08 {
   public static void main(String[] args) {
      Scanner inputan = new Scanner(System.in);
      System.out.print("Masukkan Tag: ");
      String html = inputan.nextLine();
      boolean tes = cekTagHTML(html);
      System.out.println(tes);
   }

   private static boolean cekTagHTML(String html){
      String regex = "^<([a-z]+)([^<]+)*(?:>(.*)</\\1>|\\s+/>)$";
      return html.matches(regex);
   }
}
