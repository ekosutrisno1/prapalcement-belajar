package main.regex;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Regex01 {
   public static void main(String[] args) {
      Scanner inputan = new Scanner(System.in);
      System.out.print("Masukkan username: ");
      String username = inputan.nextLine();
      boolean tes = cekNama(username);
      System.out.println(tes);

      //(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])
      //^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$
      //^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$

      //untuk query dengan kriteria tertentu dengan karakter tertentu dan dengan kondisi lain.
      //SELECT DISTINCT CITY FROM   STATION WHERE  CITY RLIKE '^[aeiouAEIOU].*[aeiouAEIOU]$';
      //SELECT DISTINCT CITY FROM STATION WHERE CITY NOT RLIKE '^[aeiouAEIOU].*$';
      //SELECT DISTINCT CITY FROM STATION WHERE CITY NOT RLIKE '^.*[aeiouAEIOU]$';
   }

   private static boolean cekNama(String username) {
      String NAME_REGEX = "^[a-z0-9_-]{3,16}$";
      Pattern pattern = Pattern.compile(NAME_REGEX, Pattern.CASE_INSENSITIVE);
      Matcher matcher = pattern.matcher(username);
      return matcher.matches();
   }
}
