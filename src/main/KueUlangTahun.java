package main;


import java.util.Scanner;

public class KueUlangTahun {
   public static void main(String[] args) {
      Scanner scan = new Scanner(System.in);
      System.out.print("Masukkan Umur Adik Anda: ");
      int n = scan.nextInt();
      int[] lilin = new int[n];

      for (int a = 0; a < n; a++) {
         lilin[a] = scan.nextInt();
      }

      int hasil = lilinKue(lilin);
      System.out.print("Adek dapat meniup ("+hasil+") Lilin");
      scan.close();
   }

   private static int lilinKue(int[] lilin) {
      int hasil = 0;
      int banyak = 0;
      for (int data : lilin) {
         if (banyak < data) {
            banyak = data;
            hasil = 1;
         } else {
            if (banyak == data) {
               hasil++;
            }
         }
      }
      return hasil;
   }

}
