package main;

import java.util.Scanner;

public class BilanganPrima01 {
   public static void main(String[] args) {
      Scanner scan = new Scanner(System.in);
      System.out.print("Masukkan Banyak: ");
      int banyak = scan.nextInt();
      bilanganPrimaInfinity(banyak);
      scan.close();
   }

   private static void bilanganPrimaInfinity(int banyak) {
      int data = 0;
      for (int a = 10; a < a + 1; a++) {
         boolean prima = true;
         for (int b = 2; b < a; b++) {
            if (a % b == 0) {
               prima = false;
               break;
            }
         }
         if (prima) {
            data++;
            if (data <= banyak) {
               System.out.print(a + " ");
            } else {
               break;
            }
         }
      }
   }
}
