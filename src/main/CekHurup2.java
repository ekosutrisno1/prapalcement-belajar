package main;

import java.util.Scanner;

public class CekHurup2 {
   public static void main(String[] args) {
      Scanner scn = new Scanner(System.in);
      System.out.print("Masukkan Kalimat: ");
      String data = scn.nextLine().toLowerCase();
      char[] input = data.toCharArray();

      System.out.print("Hurup vokal = ");
      for (char c : input) {
         if ( c == 'a' ||c == 'i' || c == 'u' ||c == 'e' || c == 'o') {
            System.out.print(c + " ");
         }
      }

      System.out.println();
      System.out.print("Hurup konsonan = ");
      for (char c : input) {
         if (!( c == 'a' ||c == 'i' || c == 'u' ||c == 'e' || c == 'o')) {
            if (c == ' ') {
               continue;
            }
            System.out.print(c + " ");
         }
      }
   }
}
