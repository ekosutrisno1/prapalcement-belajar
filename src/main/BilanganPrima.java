package main;

import java.util.Scanner;

public class BilanganPrima {
   public static void main(String[] args) {
      Scanner scan = new Scanner(System.in);
      System.out.print("Masukkan Maksimal Nilai: ");
      int bilangan = scan.nextInt();
      bilanganPrima(bilangan);
   }

   private static void bilanganPrima(int max) {
      for (int i = 2; i <= max; i++) {
         boolean prima = true;
         for (int j = 2; j < i; j++) {
            if (i % j == 0) {
               prima = false;
               break;
            }
         }
         if (prima) {
            System.out.print(i + " ");
         }
      }
   }
}
