package main;

import java.util.Scanner;

public class BilanganPrima03 {
   public static void main(String[] args) {
      Scanner scan = new Scanner(System.in);
      System.out.print("Masukkan Maksimal nilai: ");
      int nilai = scan.nextInt();
      bilanganPrimaTerbatas(nilai);
      scan.close();
   }

   private static void bilanganPrimaTerbatas(int maksimalNilai) {
      int data = 0;
      for (int a = 10; a < a + 1; a++) {
         boolean prima = true;
         for (int b = 2; b < a; b++) {
            if (a % b == 0) {
               prima = false;
               break;
            }
         }
         if (prima) {
            String x1 = Integer.toString(a);
            StringBuffer x2 = new StringBuffer(x1).reverse();
            if (cekPrima(x2.toString())) {
               data++;
               System.out.println(a + " and " + x2 + " are Match");
            }

            if (data == maksimalNilai) {
               break;
            }
         }
      }
   }

   private static boolean cekPrima(String data) {
      boolean pr = false;
      int angka = Integer.parseInt(data);
      for (int a = angka; a <= angka; a++) {
         boolean prima = true;
         for (int b = 2; b < a; b++) {
            if (a % b == 0) {
               prima = false;
               break;
            }
         }
         if (prima)
            pr = true;
      }
      return pr;
   }
}
