package main;

import java.util.Scanner;

public class Pola01 {

   public static void main(String[] args) {
      Scanner masukan = new Scanner(System.in);
      System.out.print("Masukan Data1  :");
      int input = masukan.nextInt();
      lima(input);

   }

   private static void lima(int banyak) {
      for (int i = 0; i < banyak; i++)
         if (i == 0)
            System.out.println(0);
         else if (i % 5 == 0)
            System.out.println("Five");
         else if (i % 6 == 0)
            System.out.println("Buzz");
         else
            System.out.println(i);
   }

}