package main.logic01;

import java.util.Scanner;

public class AngkaBintang01 {
   public static void main(String[] args) {
      Scanner inputan = new Scanner(System.in);
      System.out.print("Masukkan panjang : ");
      int panjang = inputan.nextInt();
      angkaBintang(panjang);
   }

   private static void angkaBintang(int panjang) {
      int a = 1;
      for (int i = 1; i <= panjang; i++) {
         if (i == 1) {
            System.out.print(a + " ");
         } else if (i % 3 == 0) {
            System.out.print("* ");
         } else {
            a += 4;
            System.out.print(a + " ");
         }
      }
   }

}
