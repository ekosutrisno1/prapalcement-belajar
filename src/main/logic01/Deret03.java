package main.logic01;

import java.util.Scanner;

public class Deret03 {
   public static void main(String[] args) {
      Scanner scan = new Scanner(System.in);
      System.out.print("Masukkan panjang angka: ");
      int angka = scan.nextInt();
      deret01(angka);
   }

   private static void deret01(int panjang) {
      int angka = 1;
      int temp;
      for (int i = 1; i <= panjang; i++) {
         if (i % 3 == 0) {
            temp = angka * -1;
            System.out.print(temp + " ");
         } else {
            System.out.print(angka + " ");
         }
         angka *= 3;
      }
   }
}
