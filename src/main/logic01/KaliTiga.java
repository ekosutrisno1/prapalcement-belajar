package main.logic01;

import java.util.Scanner;

public class KaliTiga {
   public static void main(String[] args) {
      Scanner inputan = new Scanner(System.in);
      System.out.print("Masukkan panjang : ");
      int panjang = inputan.nextInt();
      kaliDua(panjang);
   }

   private static void kaliDua(int panjang) {
      int angka = 1;
      int out;
      for (int i = 1; i <= panjang; i++) {
         if (i % 3 == 0) {
            System.out.print("* ");
         } else {
            out = angka * 4;
            System.out.print(out + " ");
            angka = out;
         }
      }
   }
}
