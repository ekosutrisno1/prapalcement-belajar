package main.logic01;

import java.util.Scanner;

public class Deret02 {
   public static void main(String[] args) {
      Scanner scan = new Scanner(System.in);
      System.out.print("Masukkan panjang angka: ");
      int angka = scan.nextInt();
      deret01(angka);
   }

   private static void deret01(int panjang) {
      int angka = 2;
      for (int i = 0; i < panjang; i++) {
         System.out.print(angka + " ");
         angka++;
      }
   }
}
