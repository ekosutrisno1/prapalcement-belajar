package main.logic01;

import java.util.Scanner;

public class KaliDua {
   public static void main(String[] args) {
      Scanner inputan = new Scanner(System.in);
      System.out.print("Masukkan panjang : ");
      int panjang = inputan.nextInt();
      kaliDua(panjang);
   }

   private static void kaliDua(int panjang) {
      int angka = 1;
      int out;
      for (int i = 0; i < panjang; i++) {
         out = angka * 2;
         System.out.print(out + " ");
         angka = out;
      }
   }
}
