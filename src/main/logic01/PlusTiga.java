package main.logic01;

import java.util.Scanner;

public class PlusTiga {
   public static void main(String[] args) {
      Scanner inputan = new Scanner(System.in);
      System.out.print("Masukkan panjang : ");
      int panjang = inputan.nextInt();
      plusTiga(panjang);
   }

   private static void plusTiga(int panjang) {
      int berhenti = 0;
      int data;
      for (int i = 0; i < i + 1; i++) {
         if (i == 0) {
            System.out.print(1 + " ");
         } else if (i > 0) {
            data = i + 3;
            System.out.print(data + " ");
            i = data - 1;
         }
         berhenti++;
         if (berhenti == panjang) break;
      }
   }
}
