package main.logic01;

import java.util.Scanner;

public class AngkaGenap {
   public static void main(String[] args) {
      Scanner inputan = new Scanner(System.in);
      System.out.print("Masukkan panjang : ");
      int panjang = inputan.nextInt();
      angkaGenap(panjang);
   }

   private static void angkaGenap(int panjang) {
      int berhenti = 0;
      for (int i = 1; i < i + 1; i++) {
         if (i % 2 != 0) {
            continue;
         }
         System.out.print(i + " ");
         berhenti++;
         if (berhenti == panjang) break;
      }
   }
}
