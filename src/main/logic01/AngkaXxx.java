package main.logic01;

import java.util.Scanner;

public class AngkaXxx {
   public static void main(String[] args) {
      Scanner scan = new Scanner(System.in);
      System.out.print("Masukkan panjang angka: ");
      int angka = scan.nextInt();
      angkaXxx(angka);
   }

   private static void angkaXxx(int panjang) {
      int angka = 1;
      int output;
      for (int i = 1; i <= panjang; i++) {
         if (i % 4 == 0) {
            System.out.print("XXX ");
         } else {
            output = angka * 3;
            System.out.print(output + " ");
            angka = output;
         }
      }
   }
}
