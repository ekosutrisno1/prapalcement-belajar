package main.logic01;

import java.util.Scanner;

public class Deret04 {
   public static void main(String[] args) {
      Scanner scan = new Scanner(System.in);
      System.out.print("Masukkan panjang angka: ");
      int angka = scan.nextInt();
      deret01(angka);
   }

   private static void deret01(int panjang) {
      int angka = 1;
      int temp = 5;
      for (int i = 1; i <= panjang; i++) {
         if (i % 2 != 0) {
            System.out.print(angka + " ");
            angka++;
         } else {
            System.out.print(temp + " ");
            temp += 5;
         }
      }
   }
}
