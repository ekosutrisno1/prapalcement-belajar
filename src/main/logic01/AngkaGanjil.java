package main.logic01;

import java.util.Scanner;

public class AngkaGanjil {
   public static void main(String[] args) {
      Scanner inputan = new Scanner(System.in);
      System.out.print("Masukkan panjang : ");
      int panjang = inputan.nextInt();
      angkaGanjil(panjang);
   }

   private static void angkaGanjil(int panjang) {
      int berhenti = 0;
      for (int i = 0; i < i + 1; i++) {
         if (i % 2 == 0) {
            continue;
         }
         System.out.print(i + " ");
         berhenti++;
         if (berhenti == panjang) break;
      }
   }
}
