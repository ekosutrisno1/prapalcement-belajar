package main;

import java.util.Scanner;

public class NaikTurun {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.print("Masukkan Hurup: ");
        String kata = scan.nextLine();
        naik_turun(kata);
        scan.close();
    }

    private static void naik_turun(String kalimat) {
        String[] kal = kalimat.split("(?<=\\G.)");
        int naik = 0;
        int turun = 0;
        int hitung = 0;
        for (String s : kal) {
            if (s.equalsIgnoreCase("N")) {
                if (hitung == 0)
                    naik++;
                hitung++;
            } else if (s.equalsIgnoreCase("T")) {

                if (hitung == 0)
                    turun++;
                hitung--;
            }
        }
        System.out.println("naik " + naik + "\nturun " + turun);
    }

}
