package main.regExs;

import java.util.Scanner;

public class ValidasiEmail {
   public static void main(String[] args) {
      Scanner inputNama = new Scanner(System.in);
      int ulang = 0;
      boolean kondisi = true;
      while (kondisi) {
         ulang++;
         System.out.print("Masukkan Nama: ");
         String email = inputNama.nextLine();

         System.out.println("Pjg: " + email.length());
         System.out.println("=> " + validasiNama(email));

         if (ulang == 5) kondisi = false;
      }
   }
  private static boolean validasiNama(String username) {
      char tes = '-', tes1 = '.', tes2 = '_';
      boolean cek1 = true, cek2 = true, cek3 = true;
      int panjang = username.length();

      for (int x = 0; x < panjang; x++) {
         if (username.charAt(x) == tes && username.charAt(x + 1) == tes) {
            cek1 = false;
            break;
         }
      }

      for (int x = 0; x < panjang; x++) {
         if (username.charAt(x) == tes1 && username.charAt(x + 1) == tes1) {
            cek2 = false;
            break;
         }
      }

      for (int x = 0; x < panjang; x++) {
         if (username.charAt(x) == tes2 && username.charAt(x + 1) == tes2) {
            cek3 = false;
            break;
         }
      }
      String regex = "^([a-zA-Z0-9._-]{3,100})@(([a-zA-Z]{2,10})|([a-zA-Z]{2,10}.[a-zA-Z]{2,10}))\\.([a-zA-Z]{2,5})$";
      return username.matches(regex) && cek1 && cek2 && cek3;
   }
}
