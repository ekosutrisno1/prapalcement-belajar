package main.regExs;

import java.util.Scanner;

public class ValidasiNama {
   public static void main(String[] args) {
      Scanner inputNama = new Scanner(System.in);
      int ulang = 0;
      boolean kondisi = true;
      while (kondisi) {
         ulang++;
         System.out.print("Masukkan Nama: ");
         String nama = inputNama.nextLine();

         System.out.println("Pjg: " + nama.length());
         System.out.println("=> " + validasiNama(nama));

         if (ulang == 5) kondisi = false;
      }
   }

   private static boolean validasiNama(String username) {
      String name = "^[a-zA-Z.,'\\s-]{3,100}$";
      return username.matches(name);
   }
}
