package main.perbankan;

import main.perbankan.models.KreditDetail;

public class JenisBungaFlat  extends KreditDetail {
   public void BungaJenisFlat(Integer angsuranKe, Double plafond, Integer jangka, Double bunga) {
      super.setAngsuranKe(angsuranKe);
      super.setJatuhTempo(angsuranKe);
      setAngsuranPokok(plafond, jangka);
      setAngsuranBunga(plafond, bunga);
      setTotalAngsuran(getAngsuranPokok() - getAngsuranBunga());
      setBaki(plafond - (getAngsuranPokok() * getAngsuranKe()));
   }

   public void setAngsuranPokok(Double plafond, Integer jangka) {
      super.setAngsuranPokok(plafond / jangka);
   }

   public void setAngsuranBunga(Double plafond, Double bunga) {
      super.setAngsuranBunga((plafond * bunga) / 1200);
   }

}
