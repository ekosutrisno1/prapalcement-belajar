package main.logic03;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

public class UsingDate {
   public static void main(String[] args) throws ParseException {
      Scanner inputTanggal = new Scanner(System.in);
      System.out.print("Masukkan Tanggal dan jam mulai: ");
      String mulai = inputTanggal.nextLine();
      System.out.print("Masukkan Tanggal dan jam akhir: ");
      String akhir = inputTanggal.nextLine();
      konvertTanggal(mulai, akhir);

      inputTanggal.close();
   }

   private static void konvertTanggal(String tanggalAwal, String tanggalAkhir) throws ParseException {
      SimpleDateFormat tanggalFormat = new SimpleDateFormat("dd-MM-yy HH:mm:ss");
      Date tanggal01 = tanggalFormat.parse(tanggalAwal);
      Date tanggal02 = tanggalFormat.parse(tanggalAkhir);

      long selisih = tanggal02.getTime() - tanggal01.getTime();

      int hari = (int) TimeUnit.MILLISECONDS.toDays(selisih);
      selisih -= TimeUnit.DAYS.toMillis(hari);

      int jam = (int) TimeUnit.MILLISECONDS.toHours(selisih);
      selisih -= TimeUnit.HOURS.toMillis(jam);

      int menit = (int) TimeUnit.MILLISECONDS.toMinutes(selisih);
      selisih -= TimeUnit.MINUTES.toMillis(menit);

      int detik = (int) TimeUnit.MILLISECONDS.toSeconds(selisih);
      System.out.printf("%d Hari %d Jam %d Menit %d Detik", hari, jam, menit, detik);
   }
}
