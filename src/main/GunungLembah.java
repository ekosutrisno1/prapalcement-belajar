package main;

import java.util.Scanner;

public class GunungLembah {

   public static void main(String[] args) {
      Scanner scan = new Scanner(System.in);
      System.out.print("Masukkan Kata: ");
      String kata = scan.nextLine();
      gunungLembah(kata);
      scan.close();
   }

   private static void gunungLembah(String kalimat) {
      String[] kata = kalimat.split("(?<=\\G.)");

      int cek1 = 0;
      int cek2 = 0;
      int gunung = 0;
      int lembah = 0;

      for (String s : kata) {
         if (s.equalsIgnoreCase("U")) {
            cek2++;
            if (cek1 - cek2 == 0) {
               gunung++;
            }
         } else if (s.equalsIgnoreCase("D")) {
            cek1++;
            if (cek2 - cek1 == 0) {
               lembah++;
            }

         }
      }
      System.out.println(
              "Telah mendaki Gunung sebanyak\t: " + lembah + " Gunung\nTelah menuruni Lembah sebanyak\t: " + gunung + " Lembah"
      );
   }
}
