package main;

import java.util.Arrays;
import java.util.Scanner;

public class Anagram {
    public static void main(String[] args) {

        Scanner scn = new Scanner(System.in);
        System.out.print("Masukkan kalimat pertama: ");
        String kal1 = scn.nextLine();

        System.out.print("Masukkan kalimat kedua: ");
        String kal2 = scn.nextLine();

        boolean s = cek_anagram(kal1, kal2);

        if (s) System.out.println("Hasil => Anagram.");
        else System.out.println("Hasil => Bukan anagram.");

        scn.close();
    }

    private static boolean cek_anagram(String kalimat1, String kalimat2) {
        String k1 = kalimat1.replace(" ", "");
        String k2 = kalimat2.replace(" ", "");

        char[] kal1 = k1.toLowerCase().toCharArray();
        char[] kal2 = k2.toLowerCase().toCharArray();

        Arrays.sort(kal1);
        Arrays.sort(kal2);

        return Arrays.equals(kal1, kal2);

    }
}
