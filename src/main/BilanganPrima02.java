package main;

import java.util.Scanner;

public class BilanganPrima02 {
   public static void main(String[] args) {
      Scanner scan = new Scanner(System.in);
      System.out.print("Masukkan Banyak: ");
      int banyak = scan.nextInt();
      System.out.print("Masukkan Maksimal nilai: ");
      int nilai = scan.nextInt();
      bilanganPrimaTerbatas(banyak, nilai);
      scan.close();
   }

   private static void bilanganPrimaTerbatas(int banyakTampil, int maksimalNilai) {
      int data = 0;
      for (int a = 10; a <= maksimalNilai; a++) {
         boolean prima = true;
         for (int b = 2; b < a; b++) {
            if (a % b == 0) {
               prima = false;
               break;
            }
         }
         if (prima) {
            data++;
            if (data <= banyakTampil) {
               System.out.print(a + " ");
            } else {
               break;
            }
         }
      }
   }
}
