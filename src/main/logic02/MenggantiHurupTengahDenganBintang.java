package main.logic02;

import java.util.Scanner;

public class MenggantiHurupTengahDenganBintang {
   public static void main(String[] args) {
      Scanner inputUser = new Scanner(System.in);
      System.out.print("Masukkan Kalimat: ");
      String kalimat = inputUser.nextLine();
      cetakKalimat(kalimat);
      inputUser.close();
   }

   private static String merubahArray(String input) {
      StringBuilder memory = new StringBuilder();
      String tengah;
      int panjang = input.length() - 1;
      char awal = input.charAt(0);
      char akhir = input.charAt(panjang);
      String kata = input.substring(1, panjang);

      for (int i = 0; i < kata.length(); i++)
         memory.append("*");

      tengah = memory.toString();
      return awal + tengah + akhir + " ";
   }

   private static void cetakKalimat(String kalimat) {
      StringBuilder kataAkhir = new StringBuilder();
      String[] data = kalimat.split(" ");

      for (String kata : data)
         kataAkhir.append(merubahArray(kata));

      System.out.print(kataAkhir + " ");
   }

}

