package main.logic02;

import java.util.Scanner;

public class JumlahKataDalamKalimat {
   public static void main(String[] args) {
      Scanner kalimat = new Scanner(System.in);
      System.out.print("Masukkan Kalimat: ");
      String kata = kalimat.nextLine();
      System.out.println(panjangArray(kata));
   }

   private static int panjangArray(String kalimat) {
      String[] s = kalimat.split(" ");
      return s.length;
   }
}
