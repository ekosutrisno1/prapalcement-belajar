package main.logic02;

import java.util.Scanner;

public class GeseerHurupKiri {
   private static Scanner scan = new Scanner(System.in);

   public static void main(String[] args) {
      allProses();
   }

   private static void allProses() {
      String raw = "abcdXYZefgXYZhij";
      String rpl1 = "XYZ", rpl2 = "@";
      StringBuilder temp = new StringBuilder();

      cetakKata("*Sebelum Geser: ");
      System.out.println(raw);

      String kata = raw.replace(rpl1, rpl2);
      char[] hurup = kata.toCharArray();

      enter();

      System.out.print("*Jumlah geser: ");
      int banyakGeser = scan.nextInt();

      enter();

      cetakKata("Setelah Geser: ");
      geserArray(hurup, banyakGeser);

      for (char h : hurup) {
         temp.append(h);
      }
      String temp2 = temp.toString();
      String result = temp2.replace(rpl2, rpl1);
      cetakKata(result);
      enter();
      scan.close();
   }

   private static void geserArray(char[] arr, int jmlGeser) {
      int panjang = arr.length - 1;
      for (int a = 0; a < jmlGeser; a++) {
         char awal = arr[0];
         for (int b = 0; b < panjang; b++) {
            arr[b] = arr[b + 1];
            arr[b + 1] = awal;
         }
      }
   }

   private static void cetakKata(String kata) {
      System.out.println(kata);
   }

   private static void enter() {
      System.out.println();
   }
}
