package main;

import java.util.Arrays;
import java.util.Scanner;

public class MembulatkanNilai {
   public static void main(String[] args) {
      Scanner scn = new Scanner(System.in);
      System.out.println( );
      System.out.print("Masukkan banyak Siswa: ");

      int siswa = scn.nextInt();
      int[] nilai = new int[siswa];

      for (int b = 0; b < siswa; b++) {
         nilai[b] = scn.nextInt();
      }

      int[] nilai_akhir = bulatNilai(nilai);
      System.out.println(Arrays.toString(nilai_akhir));
      System.out.println();
      scn.close();

   }

   private static int[] bulatNilai(int[] nilai) {
      int[] new_nilai = new int[nilai.length];
      for (int a = 0; a < nilai.length; a++) {
         if (nilai[a] % 5 > 2 && nilai[a] > 38) {
            new_nilai[a] = nilai[a] + (5 - nilai[a] % 5);
         } else {
            new_nilai[a] = nilai[a];
         }
      }
      return new_nilai;
   }
}
