package main;

public class KemungkinanPenajumlahan {
   public static void main(String[] args) {
      int angka = 100;
      peluangJumlah(angka);
   }

   private static void peluangJumlah(int angka) {
      int mulai = 1;
      int akhir = (angka + 1) / 2;

      while (mulai < akhir) {
         int jumlah = 0;
         for (int i = mulai; i <= akhir; i++) {
            jumlah += i;

            if (jumlah == angka) {
               for (int j = mulai; j <= i; j++)
                  System.out.print(j + " ");
               System.out.println();
               break;
            }
            if (jumlah > angka) break;
         }
         mulai++;
      }
   }

}
