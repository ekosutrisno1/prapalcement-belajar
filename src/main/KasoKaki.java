package main;

import java.util.Arrays;
import java.util.Scanner;

public class KasoKaki {
   public static void main(String[] args) {
      Scanner scan = new Scanner(System.in);
      System.out.print("Masukkan banyak semua kaos kaki: ");
      int banyak = scan.nextInt();
      int[] kaos_kaki = new int[banyak];
      for (int i = 0; i < banyak; i++) {
         System.out.print("Kaos ke " + (i + 1) + " :");
         kaos_kaki[i] = scan.nextInt();
      }
      int banyakPasang = banyak_pasangan(banyak, kaos_kaki);
      System.out.println("Ada (" + banyakPasang + ") pasang kaos kaki.");
      scan.close();
   }

   private static int banyak_pasangan(int x, int[] kaos_kaki) {
      int hitung_kaos = 0;
      Arrays.sort(kaos_kaki);

      for (int a = 0; a < x - 1; a++) {
         if (kaos_kaki[a] == kaos_kaki[a + 1]) {
            hitung_kaos++;
            a += 1;
         }
      }
      return hitung_kaos;
   }
}
