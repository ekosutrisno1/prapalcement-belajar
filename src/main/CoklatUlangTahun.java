package main;

import java.util.Scanner;

public class CoklatUlangTahun {
   public static void main(String[] args) {
      Scanner scan = new Scanner(System.in);
      System.out.print("Masukkan panjang coklat: ");
      int panjang = scan.nextInt();

      int[] coklat = new int[panjang];
      for (int i = 0; i < panjang; i++) {
         coklat[i] = scan.nextInt();
      }

      System.out.print("Masukkan Tanggal: ");
      int hari = scan.nextInt();

      System.out.print("Masukkan Bulan: ");
      int bulan = scan.nextInt();
      int output = coklatUltah(panjang,coklat,hari,bulan);
      System.out.println(output);
      scan.close();
   }

   private static int coklatUltah(int n, int[] isi, int tgl, int bln) {
      //membuat varaiabel
      int mulaiIndex;
      int hasil = 0;
      int total = 0;

      //loping array sebanyak n
      for (int i = 0; i < n; i++) {
         mulaiIndex = i;
         try {
            for (int j = 0; j < bln; j++) {
               total += isi[mulaiIndex + j];
            }
         } catch (Exception e) {
            break;
         }
         if (total == tgl) {
            hasil++;
         }
         total = 0;
      }
      return hasil;
   }
}
