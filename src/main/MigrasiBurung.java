package main;

import java.util.Scanner;

public class MigrasiBurung {
   public static void main(String[] args) {
      Scanner input = new Scanner(System.in);
      System.out.print("Masukkan banyak burung: ");
      int banyak = input.nextInt();
      int[] burung = new int[banyak];
      for (int a = 0; a < burung.length; a++) {
         burung[a] = input.nextInt();
      }

      int hasil = burungNarsis(banyak,burung);
      System.out.println(hasil);
      input.close();


   }

   private static int burungNarsis(int n, int[] burung) {
      int[] brg = new int[5];
      int banyak = 0;
      int result = 0;
      for (int i = 0; i < n; i++) {
         switch (burung[i]) {
            case 1:
               brg[0]++;
            case 2:
               brg[1]++;
            case 3:
               brg[2]++;
            case 4:
               brg[3]++;
            case 5:
               brg[4]++;
         }
      }

      for (int i = (brg.length - 1); i > 0; i--) {
         if (banyak <= brg[i]) {
            banyak = brg[i];
            result = i;
         }
      }
      return result;
   }
}
