package main;

import java.util.Scanner;

public class PalindromKata {
   public static void main(String[] args) {
      Scanner scan = new Scanner(System.in);
      System.out.print("Masukkan Kata: ");
      String kata = scan.nextLine();
      if (palindromKata(kata))
         System.out.println("Palindrom");
      else
         System.out.println("Bukan Palindrom");
      scan.close();
   }

   private static boolean palindromKata(String kata) {
      int a = 0, b = kata.length() - 1;
      while (a < b) {
         if (kata.charAt(a) != kata.charAt(b)) return false;
         a++;
         b--;
      }
      return true;
   }
}
