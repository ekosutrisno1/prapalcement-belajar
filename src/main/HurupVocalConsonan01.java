package main;

import java.util.Arrays;
import java.util.Scanner;

public class HurupVocalConsonan01 {
   public static void main(String[] args) {
      Scanner scan = new Scanner(System.in);
      System.out.print("Masukkan Kalimat: ");
      String kata = scan.nextLine();
      printVocalUrtan(kata);
      printConsonanUrutan(kata);
      int jVoc = jumlahVocal(kata);
      int jCon = jumlahConsonan(kata);

      System.out.println("Result=> vocal : " + jVoc + ", consonan : " + jCon + ", total : " + (jVoc + jCon) + " hurup.");
      scan.close();
   }

   // 03. melihat jumlah hurup vocal
   private static int jumlahVocal(String input) {
      char[] data = input.toCharArray();
      int bVokal = 0;
      for (char c : data) {
         if (c == 'a' || c == 'i' || c == 'u' || c == 'e' || c == 'o') {
            bVokal++;
         }
      }
      return bVokal;
   }

   //  04. melihat jumlah hurup konsonan
   private static int jumlahConsonan(String input) {
      char[] data = input.toCharArray();
      int bConsonan = 0;
      for (char c : data) {
         if (!(c == 'a' || c == 'i' || c == 'u' || c == 'e' || c == 'o')) {
            if (c == ' ' || c == '.' || c == ';' || c == '"' || c == '_' || c == ',' || c == '(' || c == ')' || c == ':' || c == '-') {
               continue;
            }
            bConsonan++;
         }
      }
      return bConsonan;
   }

   //05. Menampilkan urutan hurup vokal
   private static void printVocalUrtan(String input) {
      char[] kata = input.toCharArray();
      StringBuilder result = new StringBuilder();
      StringBuilder clock = new StringBuilder();

      for (char c : kata) {
         if (c == 'a' || c == 'i' || c == 'u' || c == 'e' || c == 'o') {
            result.append(c);
         }
      }
      String a = result.toString();
      char[] b = a.toCharArray();
      Arrays.sort(b);
      for (char out : b) {
         clock.append(out);
      }
      String d = clock.toString();
      if (d.length() == 0)
         System.out.println("Vocal: -");
      else
         System.out.println("Vocal: "+d);
   }

   // 02. menampilkan urutan hurup consonant
   private static void printConsonanUrutan(String input) {
      char[] kata = input.toCharArray();
      StringBuilder result = new StringBuilder();
      StringBuilder output = new StringBuilder();

      for (char c : kata) {
         if (!(c == 'a' || c == 'i' || c == 'u' || c == 'e' || c == 'o')) {
            if (c == ' ' || c == '.' || c == '"' || c == '_' || c == ';' || c == ',' || c == '(' || c == ')' || c == ':' || c == '-') {
               continue;
            }
            result.append(c);
         }
      }
      String a = result.toString();
      char[] b = a.toCharArray();
      Arrays.sort(b);

      for (char data : b) {
         output.append(data);
      }
      String d = output.toString();
      if (d.length() == 0)
         System.out.println("Conson: -");
      else
         System.out.println("Conson: " + d);
   }
}
