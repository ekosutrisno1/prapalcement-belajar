package main;

import java.util.Scanner;

public class Mendaki {
   public static void main(String[] args) {
      Scanner scan = new Scanner(System.in);
      System.out.print("Masukkan Karakter: ");
      String kata = scan.next();
      int hasil = mendaki(kata);
      System.out.println(hasil);
      scan.close();
   }

   private static int mendaki(String input) {
      int valley = 0;
      int level = 0;

      for (char data : input.toCharArray()) {
         if (data == 'U') ++level;
         if (data == 'D') --level;

         if (level == 0 && data == 'U') {
            valley++;
         }
      }
      return valley;
   }
}
